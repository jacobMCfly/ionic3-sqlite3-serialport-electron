"use strict";
exports.__esModule = true;
require('hazardous');
var electron_1 = require("electron");
var path = require("path");
var url = require("url");
var win, serve;
var args = process.argv.slice(1);
serve = args.some(function (val) { return val === '--serve'; });
//importar sqlite3 y fs para asignar un respectivo directorio
var sqlite3 = require('sqlite3').verbose();
var fs = require('fs');
var dir = './resources/data';
//obtener dirección de la base de datos (donde se guardara)
//var dbFile = path.join(process.resourcesPath, 'data', 'database.sqlite');  // producción
var dbFile = path.join(__dirname, 'resources', 'data', 'database.sqlite');  //develop
//crear directorio de la base de datos (en la carpeta data)
if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}
//variable para confirmar la existencia de la db
var _databaseExist = false;
var db;


// bypassing certificate errors
electron_1.app.commandLine.appendSwitch('ignore-certificate-errors');

function createWindow() {
    var electronScreen = electron_1.screen;
    var size = electronScreen.getPrimaryDisplay().workAreaSize;
    // Create the browser window.
    win = new electron_1.BrowserWindow({
        // x: 0,
        //   y: 0,
        center: true,
        width: 1000,//size.width,
        height: 800//size.height
    });
    // Open the DevTools.
    win.webContents.openDevTools();
    if (serve) {
        require('electron-reload')(__dirname, {
            electron: require(__dirname + "node_modules/electron")
        });
        win.loadURL('http://localhost:8100');
    }
    else {
        win.loadURL(url.format({
            pathname: path.join(__dirname, 'www/index.html'),
            // pathname: path.join(__dirname, 'src/index.html'),
            protocol: 'file:',
            slashes: true
        }));
    }
    // Emitted when the window is closed.
    win.on('closed', function () {
        // Dereference the window object, usually you would store window
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        win = null;
    });


    var dbExists = fs.existsSync(dbFile);

    if (!dbExists) {
        fs.openSync(dbFile, 'w');
        _databaseExist = true;
    }

}
try {

    // This method will be called when Electron has finished
    // initialization and is ready to create browser windows.
    // Some APIs can only be used after this event occurs.
    electron_1.app.on('ready', () => {
        createWindow();
        if (_databaseExist == true) {
            setTimeout(() => createDB(), 2000);
            _databaseExist = false;
        }
    });


    // Quit when all windows are closed.
    electron_1.app.on('window-all-closed', function () {
        // On OS X it is common for applications and their menu bar
        // to stay active until the user quits explicitly with Cmd + Q
        if (process.platform !== 'darwin') {
            electron_1.app.quit();
        }
    });
    electron_1.app.on('activate', function () {
        // On OS X it's common to re-create a window in the app when the
        // dock icon is clicked and there are no other windows open.
        if (win === null) {
            createWindow();
        }
    });
}
catch (e) {
    // Catch Error
    // throw e;
}

function createDB() {
    db = new sqlite3.Database(dbFile, createTables, (err) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Connected to the in-memory SQlite database.');
    });
}

async function createTables() {
    // var db = new sqlite3.Database(dbFile, (err) => { 

    await db.run('CREATE TABLE IF NOT EXISTS `Users` (' +
        '`UserId`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,' +
        '`FirstName`	TEXT NOT NULL,' +
        '`LastName`	TEXT NOT NULL,' +
        '`Age`	INTEGER NOT NULL,' +
        '`UserType`	TEXT NOT NULL,' +
        '`CreatedTime`	DATETIME DEFAULT CURRENT_TIMESTAMP,' +
        '`Password`	TEXT NOT NULL,' +
        '`UserName`	TEXT NOT NULL UNIQUE);'); //create usertable

    await db.run('CREATE TABLE IF NOT EXISTS `SignalsConfig` (' +
        '`id_sensor`	TEXT NOT NULL PRIMARY KEY UNIQUE,' +
        '`date_time`	DATETIME DEFAULT CURRENT_TIMESTAMP,' +
        '`setpoint_alto`	INTEGER,' +
        '`setpoint_bajo`	INTEGER,' +
        '`setpoint_intermedio`	INTEGER,' +
        '`alerta`	TEXT,' +
        '`mensaje`	TEXT,' +
        '`location_signal`	TEXT NOT NULL,' +
        '`group_signal`	TEXT NOT NULL ,' +
        '`sensor_name`	TEXT NOT NULL);');

    await db.run('CREATE TABLE IF NOT EXISTS `SignalsData` (' +
        '`id_signal`	TEXT NOT NULL,' +
        '`in_signal`	TEXT,' +
        '`indicator_signal`	TEXT,' +
        '`out_signal`	TEXT ,' +
        '`data_signal`	TEXT ,' +
        '`sensorName`	TEXT NOT NULL,'+
        '`sensorGroup`	TEXT NOT NULL,'+
        '`sensorLocation`	TEXT NOT NULL,'+
        '`date_time`	DATETIME DEFAULT CURRENT_TIMESTAMP,'+
        ' FOREIGN KEY(id_signal) REFERENCES SignalsConfig(id_sensor),'+
        ' FOREIGN KEY(sensorName) REFERENCES SignalsConfig(sensor_name)' +
        ' FOREIGN KEY(sensorGroup) REFERENCES SignalsConfig(group_signal)' +
        ' FOREIGN KEY(sensorLocation) REFERENCES SignalsConfig(location_signal)' +
        ');');

    db.close();
}




