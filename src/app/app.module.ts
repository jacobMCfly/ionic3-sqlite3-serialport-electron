import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/configRS232/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//services
import { ElectronService } from '../services/electron.service';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
//pages
import { LoginPage } from '../pages/login/login'; 
//graficas
import { HighchartsChartModule } from 'highcharts-angular';
//globales 
import { Globals } from '../Globals'
import { Rs232Provider } from '../providers/rs232/rs232';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    HighchartsChartModule 
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    LoginPage,
    MyApp,
    HomePage,
    ListPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ElectronService,
    AuthServiceProvider,
    Globals,
    Rs232Provider,
    Rs232Provider
  ]
})
export class AppModule {}
