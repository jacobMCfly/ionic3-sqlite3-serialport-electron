import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/configRS232/list';
import { LoginPage } from '../pages/login/login';
import { ElectronService } from '../services/electron.service';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { Events } from 'ionic-angular';
import { Globals } from "../Globals";
@Component({
  selector: 'page-menu',
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;
  clasehomepage: any = HomePage;
  pages: Array<{ title: string, component: any }>;

  constructor(public globales: Globals ,public events: Events, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public electronService: ElectronService, public authService: AuthServiceProvider) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Dashboard', component: HomePage },
      { title: 'Conexión', component: ListPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      if (this.electronService.isElectron()){
        this.electronService.checkMasterUser();
      }
      

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  async openPage(page) { 
    let changepage = await this.nav.setRoot(page.component);

    console.log(changepage);
    console.log(this.nav.getActive().name);
    if (changepage == true && this.nav.getActive().name != 'HomePage'){
     console.log("se desactivaron las graficas");
      this.globales.activate_chars =false;
    } 
  
  }
  logout() {
    this.authService.logout();
    this.nav.setRoot(LoginPage);
  }
}
