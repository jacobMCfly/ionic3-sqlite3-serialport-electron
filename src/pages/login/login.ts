import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from "../home/home";
import { FormBuilder, FormGroup } from '@angular/forms';
import { LoadingController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ElectronService } from '../../services/electron.service';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  credentialsForm: FormGroup;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public authService: AuthServiceProvider,
    public electron:ElectronService) {

    this.credentialsForm = this.formBuilder.group({
      username: [''],
      password: ['']
    });

  }


  ionViewDidLoad() {
    this.navCtrl.setRoot(HomePage);
    console.log('ionViewDidLoad LoginPage');
  }

 async login(form) {
    //console.log(form.value); 
    var resultRequest = null;
    if (form.value) {
     
    resultRequest = await this.authService.login(form.value);

      const loader = this.loadingCtrl.create({
        content: "Espere un momento..."
      });
      loader.present();

      console.log(resultRequest);
      if (resultRequest){
        this.navCtrl.setRoot(HomePage);
      }else {
        this.electron.remote.dialog.showErrorBox("Problema de autentificación","El usuario y/o contraseña son incorrectas verifique de nuevo sus credenciales");
      } 
      loader.dismiss();
    
    }

  }

}
