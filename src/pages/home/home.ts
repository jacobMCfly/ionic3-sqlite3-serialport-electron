import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { ElectronService } from '../../services/electron.service';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import Highcharts from 'highcharts';
import More from 'highcharts/highcharts-more';
More(Highcharts);
import { Events } from 'ionic-angular';
import { Globals } from "../../Globals";
declare interface dashboardConfig {
  alerta: string;
  date_time: string;
  group_signal: string;
  id_sensor: string;
  location_signal: string;
  mensaje: string;
  sensor_name: string;
  setpoint_alto: number;
  setpoint_bajo: number;
  setpoint_intermedio: number;
}

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  dashboardConfig: dashboardConfig;
  ///////////////////////////////////////////////////////////////////////////////////////////  highcharts configuración
  Highcharts = null; // required
  chartConstructor = ''; // optional string, defaults to 'chart'
  chartOptions = null;
  chartCallback = null; // optional function, defaults to null
  updateFlag = null; // optional boolean
  oneToOneFlag = null; // optional boolean, defaults to false
  runOutsideAngular = false; // optional boolean, defaults to false
  series: any = [];
  //////////////////////////////////////////////////////////////////////////////////////////


  constructor(public menuCtrl: MenuController,public globales: Globals, public navCtrl: NavController, public electronService: ElectronService, public authService: AuthServiceProvider, public events: Events) {
    this.menuCtrl.open(); 
    this.getSensorsConfig();
    this.crearGraficas();
    this.globales.activate_chars = true;
    console.log("se activaron las graficas");
  }




  // ionViewCanEnter() {
  //   return this.authService.authenticated();
  // }

  async getSensorsConfig() {
    let respuesta = await this.electronService.getSensorsConfig();
    this.dashboardConfig = respuesta;
    console.log(this.dashboardConfig);
  }


  crearGraficas() {

    this.Highcharts = Highcharts; // required 
    this.chartOptions = this.globales.chartTempOptions;
    
    var vm = this;
    this.chartCallback = (chart) => {
      setInterval(() => {
        if (vm.globales.activate_chars == true) {       // si esta activada la variable 
          if (chart.series != undefined) {
            if (chart.series[0].data.length != 0) {
              chart.series[0].setData([150]);
            }
          }
        }
      }, 300);
    }

  }






}
