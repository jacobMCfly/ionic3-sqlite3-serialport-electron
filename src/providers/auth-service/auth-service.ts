import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ElectronService } from '../../services/electron.service';


@Injectable()
export class AuthServiceProvider {
  private isLoggedIn = false; 
  constructor(public http: HttpClient, public sqlite: ElectronService) {
    console.log('Hello AuthServiceProvider Provider');
  }


  async login(credentials: any): Promise<any> {
    //console.log(credentials); 
    let resultRequest = await this.sqlite.knex("Users").select('FirstName', 'UserId').where({
      UserName: String(credentials.username),
      Password: String(credentials.password)
    }).then(
      (rows) => {
        if (rows == undefined || rows.length == 0) {
          return resultRequest = null;
        } else {
          localStorage.setItem('isLoggedIn', 'true');
          var convert = localStorage.getItem('isLoggedIn');
          this.isLoggedIn = this.getBoolean(convert);
          console.log(rows)
          return resultRequest = rows;
        }
      }).catch(
        function (error) {
          console.error(error);
        });
    return resultRequest;
  }

  // Logout a user, destroy token and remove
  // every information related to a user
  logout(): void {
    localStorage.removeItem('isLoggedIn');
    var convert = localStorage.getItem('isLoggedIn'); 
    this.isLoggedIn = this.getBoolean(convert);
    console.log(this.isLoggedIn);
  }

  // Returns whether the user is currently authenticated
  // Could check if current token is still valid
  authenticated(): boolean {
    var convert = localStorage.getItem('isLoggedIn'); 
    this.isLoggedIn = this.getBoolean(convert);
    return this.isLoggedIn;
  }


   getBoolean(value) :any{
    switch(value){
         case true:
         case "true":
         case 1:
         case "1":
         case "on":
         case "yes":
             return true;
         default: 
             return false;
     }
 }


}
