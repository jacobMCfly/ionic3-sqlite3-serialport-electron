import { Injectable } from '@angular/core';
import * as SerialPort from 'serialport';

// If you import a module but never use any of the imported values other than as TypeScript types,
// the resulting javascript file will look as if you never imported the module at all.
import { ipcRenderer, webFrame, remote } from 'electron';
import * as childProcess from 'child_process';
//import * as fs from 'fs';
import * as Knex from 'knex';
import * as path from 'path';

declare global {
  interface Window {
    require: any;
    process: any;
  }
}

@Injectable()
export class ElectronService {

  serialPort: typeof SerialPort;
  knex: typeof Knex;
  ipcRenderer: typeof ipcRenderer;
  webFrame: typeof webFrame;
  remote: typeof remote;
  childProcess: typeof childProcess;
  // fs: typeof fs;
  path: typeof path;


  constructor() {
    // Conditional imports

    if (this.isElectron()) {
      this.serialPort = window.require('serialport');
      this.knex = window.require("knex")({    //database
        client: "sqlite3",
        connection: {
          filename: './resources/data/database.sqlite' //'src/assets/data/database.sqlite'
        },
        useNullAsDefault: true,
        debug: true
      });
      this.ipcRenderer = window.require('electron').ipcRenderer;
      this.webFrame = window.require('electron').webFrame;
      this.remote = window.require('electron').remote;
      this.childProcess = window.require('child_process');
      //  this.fs = window.require('fs'); 

    }

  }

  isElectron = () => {
    return window && window.process && window.process.type;
  };

  async checkMasterUser() { // crear usuario maestro automaticamente
    if (this.isElectron()) {
      var maserExists = true;
      var UserName1 = 'master9771';

      let getmaster = this.knex('Users').where('UserName', UserName1);
      let respuesta = await getmaster;
      console.log(respuesta);
      if (!respuesta) { respuesta.catch(function (error) { console.error(error); }); return; }
      if (respuesta == undefined || respuesta.length == 0) {
        console.log("no existe");
        maserExists = false;
        this.createMasterUser(maserExists);
      } else {
        console.log("existe usuario mestro");
        maserExists = true;
      }
    }
  }

  async createMasterUser(masterExists: boolean) {
    if (this.isElectron()) {
      var firstName1 = 'master';
      var LastName1 = 'master';
      var Age1 = 27;
      var UserType1 = 'master';
      var Password1 = '461937';
      var UserName1 = 'master9771';
      if (masterExists == false) {
        console.log("creando usuario maestro");
        let createmaster = this.knex('Users').insert([
          {
            firstName: firstName1,
            LastName: LastName1,
            Age: Age1,
            UserType: UserType1,
            Password: Password1,
            UserName: UserName1
          }]);
        var respuesta = await createmaster;
        if (!respuesta) { respuesta.catch(function (error) { console.error(error); }); return; }
        if (respuesta == undefined || respuesta.length == 0) {
          console.log("no se creó usuario maestro");
        } else {
          console.log("se creo usuario maestro y se insertara configuración");
          this.insertConfigDB();
        }
      }
    }
  }

  configurationDB(): any {
    var signals_config = [
      { id_sensor: "xa45", setpoint_alto: "23", setpoint_bajo: "14", setpoint_intermedio: "18", alerta: "", mensaje: "", location_signal: "bodega", group_signal: "1", sensor_name: "sensor1" },
      { id_sensor: "xa44", setpoint_alto: "25", setpoint_bajo: "12", setpoint_intermedio: "17", alerta: "", mensaje: "", location_signal: "bodega", group_signal: "1", sensor_name: "sensor2" },
      { id_sensor: "xa46", setpoint_alto: "26", setpoint_bajo: "13", setpoint_intermedio: "16", alerta: "", mensaje: "", location_signal: "patio", group_signal: "2", sensor_name: "sensor3" },
    ];
    return signals_config;
  }

  async insertConfigDB() {
    if (this.isElectron()) {
      console.log("insertando configuración");
      var config = this.configurationDB();
      for (var i in config) {
        let insertConfig = this.knex('SignalsConfig').insert([
          {
            id_sensor: config[i].id_sensor,
            setpoint_alto: config[i].setpoint_alto,
            setpoint_bajo: config[i].setpoint_bajo,
            setpoint_intermedio: config[i].setpoint_intermedio,
            alerta: config[i].alerta,
            mensaje: config[i].mensaje,
            location_signal: config[i].location_signal,
            group_signal: config[i].group_signal,
            sensor_name: config[i].sensor_name
          }]);
        var respuesta = await insertConfig;
        if (!respuesta) { respuesta.catch(function (error) { console.error(error); }); return; }
        if (respuesta == undefined || respuesta.length == 0) {
          console.log("error al insertar configuración");
        } else {
          console.log(respuesta);
        }
      }
    }
  }

  async getSensorsConfig() {
    if (this.isElectron()) {
      let resultUsers = this.knex("SignalsConfig").select();
      let respuesta = await resultUsers;
      if (!respuesta) { respuesta.catch(function (error) { console.error(error); }); }
      if (respuesta == undefined || respuesta.length == 0) { console.log("no hay sensores") } else { return respuesta; }

      // for (var i in respuesta) {
      //   var objToString = JSON.stringify( respuesta[i]);
      //   var objtoJSON = JSON.parse(objToString[i]);
      //   localStorage.setItem(respuesta[i].id_sensor, JSON.parse(  objtoJSON ));
      // }
    }
  }
} 